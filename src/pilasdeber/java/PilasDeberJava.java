
package pilasdeber.java;
// package es el paquete en el que se encuentra el proyecto, es una agrupación de clases, interfaces enumerados entre otros

import java.io.IOException;
//permite poder interactuar entre diferentes paquetes es decir usar una clase dentro de otra clase
import javax.swing.JOptionPane;
// paquete que contiene un conjunto o grupo de clases y JoptionPane es la clase que se importa 

public class PilasDeberJava {
//hace referencia a el nombre de la clase y debe ser unico dentro del package (public;modificador de acceso, static;palabra reservada
    
    public static final int MAX_LENGTH=5;
    //Un MAX_LENGTH es la  Longitud maxima de la Pila
    //longitud de la pila, Cuando usamos “static final” se dice que creamos una constante de clase, un atributo común a todos los objetos de esa clase.
    public static String Pila1[]= new String[MAX_LENGTH];
    //Array que representa a la pila, array es un conjunto de datos del mismo tipo ordenados de forma lineal uno despues de otro, 
    public static int cima1=-1;
    //Representa la cima de la pila
    public static String Pila1aux[]= new String[MAX_LENGTH];
    //Array que representa a la pila auxiliar, int es un tipo de datos 
    public static int cima1aux=-1;
    
    public static String Pila2[]= new String[MAX_LENGTH];
    public static int cima2=-1;
    public static String Pila2aux[]= new String[MAX_LENGTH];
    public static int cima2aux=-1;
    
    
    public static void main(String[] args) throws IOException {
        //public modificador de acceso cualquier clae con acceso a mainclass puede llamarlo, static es el método estático esto quiere decir
 //que todas las instancias de esa clase comparten ese miembro en concreto, si una lo modifica, el cambio se ve en todas las instancias
 //void el metodo main no devuelve nada, main es el nombre del método, string[]args el metodo recibe como entrada un array de strings
 //throws IOException se adjunta a un encabezado / prototipo de método donde esto especifica que el método puede arrojar IOException e 
 //insistir al compilador que el bloque que está llamando a este método necesita una atención especial hacia esto con respecto a la manipulación o una nueva devolución.  
 //IOException es una clase de excepción que se generó debido a todas las contingencias de entrada / salida. 
        Menu();
        
    }

    public static void Menu() throws IOException {
         //cuando tiene la palabra void es un procedimiento cuando no es una funcion
       String salida="====Menu Principal====\n"+"1. Ingresar a Pila 1\n"+"2. Ingresar a Pila 2\n"+"3. Comparar ambas pilas\n"+"4. Salir\n";
       String entra=JOptionPane.showInputDialog (null, salida);
       int op = Integer.parseInt(entra);
       Opciones(op);
    }

    public static void Opciones(int op) throws IOException {
        
        switch (op){
            case 1: Menu1();
                    Menu();
                    break;
            
            case 2: Menu2();
                    Menu();
                    break;
                    
            case 3: Comparar();
                    Menu();
                    break;
                   
            case 4: System.exit(0);
                    break;
                    
            default: Menu();
                     break;
            
        }
  
    }
    
    public static void Menu1() throws IOException{
       String salida="====Menú de Pila 1====\n"+"1. Insertar elemento\n"+"2. Eliminar elemento\n";
       salida=salida+"3. Buscar elemento\n"+"4. Imprimir\n"+"5. Contar repetición\n";
       salida=salida+"6. Regresar a menú principal\n";
       String entra=JOptionPane.showInputDialog(null, salida);
       int op1 = Integer.parseInt(entra);
       Opciones1(op1);
        
    }
    
    public static void Menu2() throws IOException{
       String salida="====Menú de Pila 2====\n"+"1. Insertar elemento\n"+"2. Eliminar elemento\n";
       salida=salida+"3. Buscar elemento\n"+"4. Imprimir\n"+"5. Contar repetición\n";
       salida=salida+"6. Regresar a menú principal\n";
       String entra=JOptionPane.showInputDialog(null, salida);
       int op2 = Integer.parseInt(entra);
       Opciones2(op2);
        
    }
    
    public static void Opciones1(int op1)throws IOException{
        String salio1;
        switch(op1){
			case 1: Insertar1();
                                Menu1();
			        break;
			case 2: salio1=Pop1();
                                if (!Vacia1()){
                                   JOptionPane.showMessageDialog(null, "El dato que salio es "+salio1); 
                                }
                                Menu1();
			        break;
			case 3: Buscar1();
                                Menu1();
			        break;
			case 4: Imprimir1();
                                Menu1();
			        break;
                        case 5: Contar1();
                                Menu1();
			        break;
			case 6: 
			default:Menu();
			        break;
                        
	   }
    }

    public static void Insertar1() throws IOException {
       String entra = JOptionPane.showInputDialog("Digite un dato para la pila");
       Push1(entra);
    }
    
    public static void Push1(String dato)throws IOException{
      if ((Pila1.length-1)==cima1){
        JOptionPane.showMessageDialog(null,"Capacidad de la pila al limite");
        Imprimir1();
      }else{
            cima1++;
            Pila1[cima1]=dato;
      }
    }

    public static void PushAux1(String dato)throws IOException{
      if ((Pila1aux.length-1)==cima1aux){
        JOptionPane.showMessageDialog(null,"Capacidad de la pila auxiliar al limite");
      }else{
         cima1aux++;
         Pila1aux[cima1aux]=dato;
       }
    }

    public static boolean VaciaAux1(){
        return (cima1aux==-1);
    }

    public static boolean Vacia1(){
        if (cima1==-1){
            return (true);
        }
        else {
            return(false);
        }
    }

    public static String Pop1() throws IOException{
       String quedato;
      if(Vacia1()){
          JOptionPane.showMessageDialog(null,"No se puede eliminar, pila vacía !!!" );
          return("");
      }else{
              quedato=Pila1[cima1];
	      Pila1[cima1] = null;
	      --cima1;
              return(quedato);
            }
    }

    public static String PopAux1()throws IOException{
      String quedato;
      if(cima1aux== -1){
            JOptionPane.showMessageDialog(null,"No se puede eliminar, pila vacía !!!" );
            return("");
      }else{
              quedato=Pila1aux[cima1aux];
	      Pila1aux[cima1aux] = null;
	      --cima1aux;
              return(quedato);
           }
    }
   
    public static void Buscar1() throws IOException {
       if (Vacia1()){
            JOptionPane.showMessageDialog(null, "La pila esta vacìa");
        }
        else{
            String cad = JOptionPane.showInputDialog("Digite la cadena a buscar: ");
            String quedata;
            int bandera=0; //no se encuentra
            do {
                    quedata=Pop1();
                    if(cad.equals(quedata)){
                        bandera=1; //si esta
                    }
                    PushAux1(quedata);            
            }while(cima1!=-1);
            do {
                    quedata=PopAux1();
                    Push1(quedata);
            }while(cima1aux!=-1);
            if (bandera==1) {
                    JOptionPane.showMessageDialog(null,"Elemento encontrado");
            }else{
                    JOptionPane.showMessageDialog(null,"Elemento no encontrado :(");
            }
        }
    }

    public static void Imprimir1() throws IOException {
        String quedata,salida=" ";
      if (cima1!=-1)
      { do {
            quedata=Pop1();
            salida=salida+quedata+"\n"; //System.out.println mostrando
            PushAux1(quedata);            
        }while(cima1!=-1);
        do {
            quedata=PopAux1();
            Push1(quedata);
        }while(cima1aux!=-1);
        JOptionPane.showMessageDialog(null, salida);
      }
      else {
          JOptionPane.showMessageDialog(null, "La pila esta vacía");
      }
    }

    public static void Contar1() throws IOException {
        String quedata;
        int contador = 0;
        if (cima1!=-1)
          { do {
                quedata=Pop1();
                contador = contador+1;
                PushAux1(quedata);            
            }while(cima1!=-1);
            do {
                quedata=PopAux1();
                Push1(quedata);
            }while(cima1aux!=-1);
            JOptionPane.showMessageDialog(null,"Elementos en la pila: "+ contador);
          }
        else {
            JOptionPane.showMessageDialog(null, "La pila esta vacìa");
        }
    }
    
    
    public static void Opciones2(int op2)throws IOException{
        String salio2;
        switch(op2){
			case 1: Insertar2();
                                Menu2();
			        break;
			case 2: salio2=Pop2();
                                if (!Vacia2()){
                                   JOptionPane.showMessageDialog(null, "El dato que salio es "+salio2); 
                                }
                                Menu2();
			        break;
			case 3: Buscar2();
                                Menu2();
			        break;
			case 4: Imprimir2();
                                Menu2();
			        break;
                        case 5: Contar2();
                                Menu2();
			        break;
			case 6: 
			default:Menu();
			        break;
                        
	   }
    }

    public static void Insertar2() throws IOException {
       String entra = JOptionPane.showInputDialog("Digite un dato para la pila");
       Push2(entra);
    }
    
    public static void Push2(String dato)throws IOException{
      if ((Pila2.length-1)==cima2){
        JOptionPane.showMessageDialog(null,"Capacidad de la pila al limite");
        Imprimir2();
      }else{
            cima2++;
            Pila2[cima2]=dato;
      }
    }

    public static void PushAux2(String dato)throws IOException{
      if ((Pila2aux.length-1)==cima2aux){
        JOptionPane.showMessageDialog(null,"Capacidad de la pila auxiliar al limite");
      }else{
         cima2aux++;
         Pila2aux[cima2aux]=dato;
       }
    }

    public static boolean VaciaAux2(){
        return (cima2aux==-1);
    }

    public static boolean Vacia2(){
        if (cima2==-1){
            return (true);
        }
        else {
            return(false);
        }
    }

    public static String Pop2() throws IOException{
       String quedato;
      if(Vacia2()){
          JOptionPane.showMessageDialog(null,"No se puede eliminar, pila vacía !!!" );
          return("");
      }else{
              quedato=Pila2[cima2];
	      Pila2[cima2] = null;
	      --cima2;
              return(quedato);
            }
    }

    public static String PopAux2()throws IOException{
      String quedato;
      if(cima2aux== -1){
            JOptionPane.showMessageDialog(null,"No se puede eliminar, pila vacía !!!" );
            return("");
      }else{
              quedato=Pila2aux[cima2aux];
	      Pila2aux[cima2aux] = null;
	      --cima2aux;
              return(quedato);
           }
    }
   
    public static void Buscar2() throws IOException {
       if (Vacia2()){
            JOptionPane.showMessageDialog(null, "La pila esta vacìa");
        }
        else{
            String cad = JOptionPane.showInputDialog("Digite la cadena a buscar: ");
            String quedata;
            int bandera=0; //no se encuentra
            do {
                    quedata=Pop1();
                    if(cad.equals(quedata)){
                        bandera=1; //si esta
                    }
                    PushAux1(quedata);            
            }while(cima2!=-1);
            do {
                    quedata=PopAux1();
                    Push1(quedata);
            }while(cima2aux!=-1);
            if (bandera==1) {
                    JOptionPane.showMessageDialog(null,"Elemento encontrado");
            }else{
                    JOptionPane.showMessageDialog(null,"Elemento no encontrado :(");
            }
        }
    }

    public static void Imprimir2() throws IOException {
        String quedata,salida=" ";
      if (cima2!=-1)
      { do {
            quedata=Pop2();
            salida=salida+quedata+"\n"; //System.out.println mostrando
            PushAux2(quedata);            
        }while(cima2!=-1);
        do {
            quedata=PopAux2();
            Push2(quedata);
        }while(cima2aux!=-1);
        JOptionPane.showMessageDialog(null, salida);
      }
      else {
          JOptionPane.showMessageDialog(null, "La pila esta vacía");
      }
    }

    public static void Contar2() throws IOException {
        String quedata;
        int contador = 0;
        if (cima2!=-1)
          { do {
                quedata=Pop2();
                contador = contador+1;
                PushAux2(quedata);            
            }while(cima2!=-1);
            do {
                quedata=PopAux2();
                Push2(quedata);
            }while(cima2aux!=-1);
            JOptionPane.showMessageDialog(null,"Elementos en la pila: "+ contador);
          }
        else {
            JOptionPane.showMessageDialog(null, "La pila esta vacìa");
        }
    }
     public static boolean Comparar() {
         if(Vacia1()&&Vacia2()){
            JOptionPane.showMessageDialog(null,"Las pilas estan vacías" );
            return true;
        }
         else if ((cima2==cima1)&&(cima1==cima2)){
            JOptionPane.showMessageDialog(null,"Las Pilas tienen la misma cantidad de elementos, pero no son iguales ");
            return true;
        }else
            JOptionPane.showMessageDialog(null,"Las Pilas no son iguales ");
            return false;
    }
         
}
     
/*else if (Pila1==Pila2 && Pila2==Pila1){
             JOptionPane.showMessageDialog(null,"Las Pilas son iguales");
             return true;
         */